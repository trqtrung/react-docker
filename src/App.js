import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";
import logo from './logo.svg';
import './App.css';

import Home from './components/home/home';
import Note from './components/note/note';

function App() {
  return (
    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <h3>Hello boss tiemvattu.com</h3>
    //     <h4>This app is automated deploy on ubuntu server via docker on every commit</h4>
    //     <h5>That's cool!</h5>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       React Docker Ubuntu
    //     </a>
    //   </header>
    // </div>
    <Router>
      <div>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/note">Note</Link>
          </li>
        </ul>

        <Switch>
          <Route path="/note">
            <Note />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
